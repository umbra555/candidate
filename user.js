const uuid = require('uuid');
const UserStatus = require('./userStatus');

function getDataInt() {
    return Math.trunc(new Date().getTime() / 1000)
}

class User {

    constructor(login) {
        this.user = {
            login: login,
            passw: '',
            status: UserStatus.free,
            timeStart: 0,
            token: '',
            candidateName: '',
        }

    }

    restoreUser(userData) {
        this.user = userData;
        return this;
    }

    getCandidateName() {
        return this.user.candidateName;
    }

    setCandidateName(candidateName) {
        this.user.candidateName = candidateName;
    }

    getWorkTime() {
        //console.log(`Получаем время: ${getDataInt() - this.user.timeStart}`);
        return getDataInt() - this.user.timeStart;
    }

    getLogin() {
        return this.user.login;
    }

    getStatus() {
        return this.user.status;
    }

    genPasswd(passw) {
        this.user.passw = uuid().slice(0, 12);
    }

    setStatus(status) {

        this.user.status = status;
    }

    setTimeStart() {
        this.user.timeStart = getDataInt();
    }

    genToken() {
        this.user.token = uuid();
    }

    getToken() {
        return this.user.token;
    }

    getPasswd() {
        return this.user.passw;
    }

}

module.exports = User;