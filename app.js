const storage = require('./storage');
const hanlder = require('./handlers');
const Express = require('express');
const config = require('./config.json');

const PORT = config.PORT;

const SECRET = 'jdfh^78df65dfjd%65d8f6dhjfdJHf^^76'; //todo: move to cfg

const app = Express();

app.use(require('body-parser').json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
    next();
});

app.use((req, res, next) => {

    if (req.method != 'OPTIONS' && !/activeToken?/.test(req.url) && req.get('authorization') != SECRET) {
        console.log(`Отклонен запрос: ${req.url} секрет: ${req.get('authorization')}`);
        res.status(500);
    }

    next();
});

app.post('/resetUser', (req, res) => {

    let login = req.body.login;

    console.log(`Запрос на сброс учетки: ${login}`);

    if (!login) {
        res.sendStatus(400);
        return 1;
    }

    hanlder.disableUser(storage.getUserByLogin(login));
    storage.resetUser(login);

    res.sendStatus(200);

});

app.post('/createNewToken', (req, res) => {

    let email = req.body.email;
    let name = req.body.name;
    let login = req.body.activeUser;

    console.log(`Запрос на создание токена email: ${email} name: ${name}`);

    if (!email || !name || !login) {
        res.sendStatus(400);
        return 1;
    }

    res.send({
        result: hanlder.newToken(email, name, login)
    });
});

app.post('/activeToken', (req, res) => {

    let token = req.body.token;

    if (!token) {
        res.sendStatus(400);
        return 1;
    }

    res.send({
        result: hanlder.activeToken(token)
    });
});

app.post('/getWhaitngUsers', (req, res) => {
    res.send({
        result: storage.getWhaitngUsers()
    });
});

app.post('/getWorkingUsers', (req, res) => {
    res.send({
        result: storage.getWorkingUsers()
    });
});

app.post('/getEndedUsers', (req, res) => {
    res.send({
        result: storage.getEndedUsers()
    });
});

app.post('/getAllUsers', (req, res) => {

    let result = [];

    storage.getAllUsers().forEach(user => {
        result.push({
            login: user.getLogin(),
            status: user.getStatus(),
            name: user.getCandidateName(),
        });
    });

    res.send({
        result: result
    });
});

app.get('/activeToken', (req, res) => {

    let token = req.query.token;

    if (!token) {
        res.send('Ошибка активации, токен не задан!');
        return;
    }

    let pass = hanlder.activeToken(token);

    let result = `Активация токена: ${token} <BR>`;

    if (pass == 1) {
        result += `Ошибка! Токен не существует или был активирован ранее!`;
    } else {
        result += `Доступ по RDP => 188.43.101.137:10005 <BR>`;
        result += `Имя пользователя: ${storage.getUserByToken(token).getLogin()} <BR>`;
        result += `Пароль: ${pass}`;
    }

    res.send(result);
});

setInterval(() => {
    hanlder.timeControl();
}, 1000);

app.listen(PORT, () => {
    console.log(`Listen on port ${PORT}...`);
})