"use strict";
const nodemailer = require("nodemailer");

const account = require('./email.json');
const from = '"OCRV" <ocrv.candidate@ya.ru>';

const task = [];

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, 
	connectionTimeout: 30000,
    auth: {
      user: account.login, 
      pass: account.pass 
    }
  });


setInterval( () => {
	
	task.forEach( (el, index) => {
    
    if (el.status != 1)  {
      return;
    }

    el.status = 2;

    transporter.verify(function(error, success) {
		  if (error) {
			  console.log(error);
		  } else {
        console.log("Server is ready to take our messages");


        transporter.sendMail(el.msg)
        .then( result => {
          console.log(result) 
          task.splice(index, 1);
        })
        .catch( e => {
            console.log(e)
            el.status = 3;
          });
        }
		});
	});
}, 3000);

async function sendEmail(to, subject, html){
  	
	const msg = {
		to: to, 
		subject: subject, 
		html: html//,
		//attachments: [{ path: 'attach/task1C.pdf' }]
	}
	
	task.push({status: 1, msg: msg});
		
}

module.exports = sendEmail;