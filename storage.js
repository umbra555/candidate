const User = require('./user');
const UserStatus = require('./userStatus');
const fs = require('fs');

const dataFile = './storage.dat';

class Storage {

    constructor() {
        this.users = [
            new User('rdsUser012'),
            new User('rdsUser013'),
            new User('rdsUser014'),
        ];

        this.fileBlock = false;

        this.readFromFile();
    }

    readFromFile() {
        fs.readFile(dataFile, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {

                let users;

                try {
                    users = JSON.parse(data);
                } catch (e) {
                    console.log(e);
                }

                if (!users) {
                    console.log('Ошибка обработки состояния');
                    return;
                }

                this.users = [];

                users.forEach(el => {
                    this.users.push(new User().restoreUser(el.user));
                    //console.log(new User().restoreUser(el.user))
                })

                console.log('Состояние восстановлено');
            }
        })
    }

    saveStateToFile() {
        let strState = JSON.stringify(this.users);

        if (!this.fileBlock) {
            this.fileBlock = true;
            fs.writeFile(dataFile, strState, err => {
                if (err) console.log(`Ошибка записи состояния: ${err}`)
                else {
                    this.fileBlock = false;
                    console.log(`Состояние записано`)
                }
            });
        } else {
            setTimeout(() => {
                this.saveStateToFile();
                console.log('Файл заблокирован повторная попытка')
            }, 1000);
        }
    }

    resetUser(login) {
        let users = this.users.filter(user => user.getLogin() == login);

        if (users.length) {
            let user = users[0];

            user.setStatus(UserStatus.free);
            user.setCandidateName('');

        }

        this.saveStateToFile();
    }

    getUserByLogin(login) {
        let users = this.users.filter(user => user.getLogin() == login);

        if (users.length) {
            let user = users[0];

            return user;
        }
    }

    getFreeUserRecord() {
        let freeUsers = this.users.filter(user => user.getStatus() == UserStatus.free);

        if (freeUsers.length) {
            return freeUsers[0];
        }

    }

    getUserByToken(token) {
        let freeUsers = this.users.filter(user => user.getToken() == token);

        if (freeUsers.length) {
            return freeUsers[0];
        }
    }


    getWhaitngUsers() {
        return this.getUserByStatus(UserStatus.whaiting);
    }

    getWorkingUsers() {
        return this.getUserByStatus(UserStatus.wroking);
    }

    getEndedUsers() {
        return this.getUserByStatus(UserStatus.end);
    }

    getUserByStatus(userStatus) {
        return this.users.filter(user => user.getStatus() == userStatus);
    }

    getAllUsers() {
        return this.users;
    }

}

module.exports = new Storage();