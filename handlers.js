const storage = require('./storage');
const UserStatus = require('./userStatus');
const cmd = require('./cmd');
const sendToEmail = require('./email');
const config = require('./config.json');

const API_ADDR = config.API_ADDR;
const MAX_WORK_TIME = config.MAX_WORK_TIME;

function newToken(email, name, login) {
    //let user = storage.getFreeUserRecord();\
    let user = storage.getUserByLogin(login);

    if (user.getStatus() != UserStatus.free) {
        console.log(`Ошибка, пользовател ${user.getLogin()} занят!`);
        return 1;
    }

    const subj = 'Приглашение выполнить тестовое задание OCRV';

    if (!user) {
        console.log('all user busy');
        return 1;
    }

    user.genToken();
    user.setStatus(UserStatus.whaiting);
    user.genPasswd();
    user.setCandidateName(name);

    const html = `
    <h1>Добрый день, ${name}</h1>

    Вы получили приглашение пройти тестовое задание ОЦРВ. <br>
	<br>
    В удобное для вас время перейдите по ссылке и активируйте токен: <br>
    <a href="${API_ADDR}${user.getToken()}">Активировать токен ${user.getToken()}</a><br>
	<br>
    После активации Вам будет выдан логин и пароль для доступа в систему, <br>
    а так же начнётся отсчет времени.<br>
	<br>
	Задание находится в конфигурации в общем макете ЗаданиеДляКандидата<br>

    По окнчанию 4 часов сессия завершится и логин будет заблокирован.<br>
    `;

    sendToEmail(email, subj, html);
    cmd.setUserAccount(user.getLogin(), 0);
    cmd.setUserPass(user.getLogin(), user.getPasswd());
    cmd.logoff(user.getLogin());

    console.log(`Зарезервирован пользователь ${user.getLogin()} пароль ${user.getPasswd()}`);

    storage.saveStateToFile();

    return 0;

}

function activeToken(token) {

    let user = storage.getUserByToken(token);

    if (!user) {
        console.log(`Не найден активный токен ${token}`);
        return 1;
    }


    console.log(`Пользвоатель ${user.getLogin()} активировал свой токен.`);
    cmd.setUserAccount(user.getLogin(), 1);
    //sendToEmail

    user.setStatus(UserStatus.wroking);
    user.setTimeStart();

    storage.saveStateToFile();

    return user.getPasswd();

}

function disableUser(user) {
    cmd.setUserAccount(user.getLogin(), 0);
    cmd.logoff(user.getLogin());
    user.setStatus(UserStatus.end);
    //user.setCandidateName('');
    storage.saveStateToFile();
}

function timeControl() {

    storage.getAllUsers().filter(user => user.getStatus() == UserStatus.wroking)
        .forEach(user => {
            if (user.getWorkTime() > MAX_WORK_TIME) {
                disableUser(user);
            }
        });



}

module.exports = {
    newToken: newToken,
    activeToken: activeToken,
    timeControl: timeControl,
    disableUser: disableUser
}