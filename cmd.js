const cmd = require('node-cmd');


function setUserAccount(userName, status) {

    let active;

    if (status == 0) {
        active = 'no';
    } else {
        active = 'yes';
    }

    cmd.run(`net user ${userName} /active:${active}`);
}


function logoff(userName) {
    
    cmd.get(
        `quser ${userName}`,
        (err, data, stderr) => {
            
            let arr = data.split(/\s+/);

            if (arr.length) {

                let sessionId = arr[10];
                
                if (sessionId) {
                    cmd.run(`logoff ${sessionId}`);
                }

            } else {
                console.log('ПОльзвоатель не найден')
            }

        });
}

function setUserPass(userName, pass) {
    cmd.run(`net user ${userName} ${pass}`);  
}

module.exports = {
    logoff: logoff,
    setUserAccount: setUserAccount,
    setUserPass: setUserPass,
}